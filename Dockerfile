FROM node:16

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --production

COPY index.html index.mjs ./

VOLUME [ "/project" ]
EXPOSE 3000

CMD [ "node", "index.mjs" ]

