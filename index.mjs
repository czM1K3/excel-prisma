import express from "express";
import { fileURLToPath } from 'url';
import { dirname } from "path";
import multer from "multer";
import { writeFileSync } from "fs";
import { spawn } from "child_process";

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const app = express();

const upload = multer({
	limits: {
		fileSize: 50 * 1024 * 1024,
	},
});

app.get("/", (_req, res) => {
	res.sendFile(`${__dirname}/index.html`);
});

app.post("/", upload.single("file"), async (req, res) => {
	req.setTimeout(1000 * 60 * 5);
	try {
		const buffer = req.file.buffer;
		writeFileSync(`/project/prisma/seed/sample.xlsx`, buffer);
		
		await new Promise((resolve, reject) => {
			const ls = spawn("bash", ["-c", "cd /project && yarn install --frozen-lockfile && printf 'y\n' yarn prisma:reset"]);
	
			ls.stdout.on("data", (data) => {
				res.write(data);
			});
	
			ls.stderr.on("data", (data) => {
				res.write(data);
			});
	
			ls.on("error", () => {
				reject();
			});
	
			ls.on("close", () => {
				resolve();
			});
		});
	} catch (e) {
		res.send("Error");
	}
	res.end();
});

app.listen(3000, () => {
	console.log("Server is listening on http://localhost:3000");
});
